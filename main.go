/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package main

import "vrc-rpc-tool/cmd"

func main() {
	cmd.Execute()
}
